import React,{Component} from 'react';
import {render} from 'react-dom';

var data = [
    { date:'01.01.2018', price:100 },
    { date:'02.01.2018', price:200 },
    { date:'03.01.2018', price:300 },
    { date:'04.01.2018', price:100 },
    { date:'05.01.2018', price:200 },
    { date:'06.01.2018', price:300 },
    { date:'07.01.2018', price:100 },
    { date:'08.01.2018', price:200 },
    { date:'09.01.2018', price:300 },
    { date:'10.01.2018', price:100 },
    { date:'11.01.2018', price:200 },
    { date:'12.01.2018', price:300 },
    { date:'13.01.2018', price:100 },
    { date:'14.01.2018', price:200 },
    { date:'15.01.2018', price:300 },
    { date:'16.01.2018', price:100 },
    { date:'17.01.2018', price:200 },
    { date:'18.01.2018', price:300 },
    { date:'19.01.2018', price:100 },
    { date:'20.01.2018', price:200 },
    { date:'21.01.2018', price:300 },
    { date:'22.01.2018', price:100 },
    { date:'23.01.2018', price:200 },
    { date:'24.01.2018', price:300 },
    { date:'25.01.2018', price:100 },
    { date:'26.01.2018', price:200 },
    { date:'27.01.2018', price:300 },
    { date:'28.01.2018', price:100 },
    { date:'29.01.2018', price:200 },
    { date:'30.01.2018', price:300 },
];

class Widget extends Component{
    
    constructor(props){
        super();
        this.state = {
            data:props.data,
            limit:18,
            offset:0,
            active:-1,
            days:['ВС','ПН','ВТ','СР','ЧТ','ПТ','СБ'],
            month:['январь','февраль','март','апрель','март','июнь','июль','август','сентябрь','октябрь','ноябрь','декабрь'],
            barHeight:100
        }
    }
    goToPrev=()=>{
        if(this.state.offset<1) return false;
        this.setState({offset:this.state.offset - 1});
    }
    goToNext=()=>{
        console.log( this.state.offset, this.state.data.length );
        if(this.state.limit + this.state.offset > this.state.data.length -1) return false;
        this.setState({offset:this.state.offset + 1});
    }
    render(){

        var itemStyle = {
            display:'inline-block',
            margin:'0px 5px'
        };
        
        var itemBarStyle = height => {
            return {
                width:'100%',
                height:`${ this.state.barHeight / Math.max.apply(null,this.state.data.map(e=>e.price)) * height}px`,
                background:'#dedede'
            }
        };

        console.log( 'max', this.state.barHeight , Math.max.apply( null, this.state.data.map(e=>e.price) ) );

        return(<div className="widget-container">
            <div> { this.state.month[ (~~this.state.data[0].date.split('.')[1]) - 1 ] } </div>
            <div>
            <button onClick={this.goToPrev} style={itemStyle}> - </button>
            
            <div className="widget-content" style={itemStyle}>
                {this.state.data.slice(this.state.offset,this.state.limit + this.state.offset).map(e=>{
                    return (
                        <div key={e.date} className="widget-item" style={itemStyle}>
                            <div className="price" style={ itemBarStyle(e.price) }> {e.price} </div>
                            <div className="day-week"> { this.state.days[ (new Date( e.date.split('.').reverse().join('-') )).getDay() ] } </div>
                            <div className="day-month"> { e.date.split('.')[0] } </div>
                        </div> 
                    );
                })}
            </div>

            <button onClick={this.goToNext} style={itemStyle}> + </button>
            </div>

        </div>);
    }

}

class App extends Component{
    render(){
        return(
            <Widget data={data}/>
        );
    }
}

render(<App/>,document.getElementById('root'));